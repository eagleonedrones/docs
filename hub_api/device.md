# Device [/device]

## Create new [POST]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "id": "123",
                "type": "drone",
                "name": "Device name",
                "id_user": "1",
                "id_group": "5"
            }
        }

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "type": "drone",
                "name": "Device name",
                "files_count": 0,
                "id_user": "1",
                "id_group": "5"
            }
        }

## Get all [GET /device?load_linked_objects={load_linked_objects}]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + load_linked_objects (bool)
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "type": "drone",
                    "name": "Device name",
                    "files_count": 0,
                    "id_user": "1",
                    "id_group": "0",
                    "user" : {
                        "id": 123,
                        "email": "email@email.com",
                        "groups_count": 12,
                        "sessions_count": 5,
                        "permissions": ["view"]
                    },
                    "group": {}
                }
            ]
        }

## Device - Object [/device/{id_device}]

### Get [GET]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_device (string)
        
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "type": "drone",
                "name": "Device name",
                "files_count": 0,
                "id_user": "1",
                "id_group": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Patch [PATCH]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "id_user": "5",
                "name": "New name",
                "type": "drone"
            }
        }
        
+ Parameters
    + id_device (bool)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "type": "drone",
                "name": "New name",
                "files_count": 0,
                "id_user": "5",
                "id_group": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Update [PUT]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "id_user": "5",
                "name": "New name",
                "type": "drone"
            }
        }
        
+ Parameters
    + id_device (bool)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "type": "drone",
                "name": "New name",
                "files_count": 0,
                "id_user": "5",
                "id_group": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Delete [DELETE]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_device (numeric)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }
        
## Device - linking other objects [/device/{id_device}/]

Get files related to the device.

### Get files [GET /device/{id_device}/file]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_device (numeric)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "name": "Log file 1.1.1",
                    "size": "456434849",
                    "timestamp": "2018-01-25 15:32:20",
                    "url": "https://path/to/file.log",
                    "id_user": "1",
                    "id_device": "5",
                    "permissions": ["view", "edit", "delete", "assign"]
                }
            ]
        }

