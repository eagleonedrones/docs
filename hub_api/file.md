# File [/file]

## Create new [POST]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "name": "report_log.log",
                "id_user": "1",
                "id_device": "0"
            }
        }

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "abc123",
                "url": "http://path/to/upload/file",
                "id_user": "1",
                "id_device": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

## Get all [GET /file?load_linked_objects={load_linked_objects}]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + load_linked_objects (bool)
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "name": "Log file 1.1.1",
                    "size": "456434849",
                    "timestamp": "2018-01-25 15:32:20",
                    "url": "https://path/to/file.log",
                    "id_user": "1",
                    "id_device": "0",
                    "user": {
                        "id": "123",
                        "email": "email@email.email",
                        "groups_count": 1,
                        "sessions_count": 4,
                        "role": "user",
                        "permissions": ["view", "edit", "delete"]
                    },
                    "device": {},
                    "permissions": ["view", "edit", "delete", "assign"]
                }
            ]
        }

## File - Object [/file/{id_file}]

### Get [GET]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_file (string)
        
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "name": "Log file 1.1.1",
                "size": "456434849",
                "timestamp": "2018-01-25 15:32:20",
                "url": "https://path/to/file.log",
                "id_user": "1",
                "id_device": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Patch [PATCH]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "name": "Log file 1.1.1",
                "id_user": "1",
                "id_device": "0"
            }
        }
        
+ Parameters
    + id_file (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "name": "Log file 1.1.1",
                "size": "456434849",
                "timestamp": "2018-01-25 15:32:20",
                "url": "https://path/to/file.log",
                "id_user": "1",
                "id_device": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Update [PUT]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "name": "Log file 1.1.1",
                "id_user": "1",
                "id_device": "0"
            }
        }
        
+ Parameters
    + id_file (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "name": "Log file 1.1.1",
                "size": "456434849",
                "timestamp": "2018-01-25 15:32:20",
                "url": "https://path/to/file.log",
                "id_user": "1",
                "id_device": "0",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Delete [DELETE]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_file (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }

