# Authorization [/get_token/]

Auth token can be obtained only for mobile app access - other devices have tokens pre-generated.

All requests, except the one for getting the token itself (`/get_token/`) must contain authorization header:

```
Authorization: Basic {auth_token}
```


# Obtaining auth token [POST]

+ Request (application/json)

        {
            "email": "email@email.email",
            "password": "passWord123"
        }
    
+ Response 200 (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "valid_utill": 1559235026
        }

