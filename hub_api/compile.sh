#!/usr/bin/env bash

touch api.md

cat general.md > api.md
cat auth.md >> api.md
cat group.md >> api.md
cat device.md >> api.md
cat user.md >> api.md
cat file.md >> api.md