# Members [/companies/{company_id}/members/]

## Create new [POST]

+ Request (application/json)

        {
            "email": "email@email.email",
            "password": "PassWord123789"
        }

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "email": "email@email.email",
                "groups_count": 0,
                "sessions_count": 0,
                "role": "user",
                "permissions": ["view", "edit", "delete"]
            }
        }

## Get all [GET]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "email": "email@email.email",
                    "groups_count": 2,
                    "sessions_count": 1,
                    "role": "user",
                    "permissions": ["view"]
                },
                {
                    "id": "456",
                    "email": "second@email.email",
                    "groups_count": 5,
                    "sessions_count": 2,
                    "role": "user",
                    "permissions": ["view", "edit", "delete"]
                }
            ]
        }

## User - Object [/user/{id_user}]

### Get [GET]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)
        
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "456",
                "email": "second@email.email",
                "groups_count": 5,
                "sessions_count": 2,
                "role": "user",
                "permissions": ["view", "edit", "delete"]
            }
        }

### Patch [PATCH]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "email": "third@email.email",
                "role": "user"
            }
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "456",
                "email": "third@email.email",
                "groups_count": 5,
                "sessions_count": 2,
                "role": "user",
                "permissions": ["view", "edit", "delete"]
            }
        }

### Update [PUT]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "email": "third@email.email",
                "role": "user"
            }
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "456",
                "email": "third@email.email",
                "groups_count": 5,
                "sessions_count": 2,
                "role": "user",
                "permissions": ["view", "edit", "delete"]
            }
        }

### Delete [DELETE]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }
        
## User - password [/user/{id_user}/password]

User's password management endpoints.

### Change password [PATCH /user/{id_user}/password]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": [
                "old_password": "",
                "new_password": ""
            ]
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }
        
### Reset password [POST /user/{id_user}/password]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }

## User - Objects linking [/user/{id_user}/]
Get other objects associated with the user.

### Get devices [GET /user/{id_user}/device]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                todo
            ]
        }
        
### Get groups [GET /user/{id_user}/group]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "1",
                    "name": "Group #1",
                    "location": {
                        "sw": {
                            "lat": 50.0964759,
                            "lng": 14.3937703
                        },
                        "ne": {
                            "lat": 50.1019541,
                            "lng": 14.4088336
                        }
                    }, 
                    "members_count": 5,
                    "devices_count": 3,
                    "id_user": "1",
                    "user": {},
                    "permissions": ["view", "edit", "delete", "assign"]
                },
                {
                    "id": "2",
                    "name": "Group #2",
                    "location": {
                        "sw": {
                            "lat": 50.0964759,
                            "lng": 14.3937703
                        },
                        "ne": {
                            "lat": 50.1019541,
                            "lng": 14.4088336
                        }
                    }, 
                    "members_count": 1,
                    "devices_count": 0,
                    "id_user": "1",
                    "permissions": ["view", "edit", "delete", "assign"]
                }
            ]
        }
        
### Get sessions [GET /user/{id_user}/session]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "156484",
                    "name": "iPhone X",
                    "active_since": 1549633710,
                    "last_active": 1549783710,
                    "permissions": ["view", "delete"]
                },
                {
                    "id": "156442",
                    "name": "iPad",
                    "active_since": 1549633710,
                    "last_active": 1549783710,
                    "permissions": ["view", "delete"]
                },
                {
                    "id": "156484",
                    "name": "Ubuntu 18.04",
                    "active_since": 1549633710,
                    "last_active": 1549783710,
                    "permissions": ["view", "delete"]
                }
            ]
        }
        
### Delete sessions [DELETE /user/{id_user}/session]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }

### Delete session [DELETE /user/{id_user}/session/{id_session}]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_user (string)
    + id_session (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }

