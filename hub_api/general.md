FORMAT: 1A
HOST: https://hub.eagle.one

#TODO - modify host url?


# Eagle.One Hub

API for Eagle.One hub. 

Version 1.0


## Allowed HTTPs requests:

<pre>
GET     : Get a resource or list of resources
POST    : Create new resource
PUT     : Update a resource
PATCH   : Edit a resource
DELETE  : Remove a resource
</pre>

*note:* To use `PUT` request all data fields have to be provided, while using the `PATCH` request,
only provided fields will be updated.


## Status Codes for server responses:

- 200 `OK` - the request was successful,
- 400 `Bad Request` - the request could not be understood or was missing required parameters,
- 401 `Unauthorized` - authentication failed or user doesn't have permissions for requested operation,
- 403 `Forbidden` - access denied,
- 404 `Not Found` - resource was not found,
- 5XX `Internal error` - an unexpected error has occurred.


<!--
## Permissions types:

- `view` - user can view this object,
- `edit` - user can edit this object,
- `delete` - user can delete this object,
- `owner` - user can assign this object to other users.


## Error responses:

All these error responses are returned with status code `400`.
- `error.api_error` - something went wrong, internal error,
- `error.timeout` - the request has timed out, tyring again is recommended,
- `error.not_permitted` - the user does not have permissions for the request,
- `error.missing_fields` - the request is missing some required fields.


## Data format

All **bool** variables are represented in numeric format (1 or 0).


## Request

Body of every request needs to be a valid JSON string in the format listed below. All parameters are required (including
`data` even if it is empty dictionary).

```json
{
  "auth_token": "",
  "timestamp": 1549292026,
  "data": {}
}
```

- `auth_token` **string**, authentication token of the user sending the request (ath_token is obtained by calling the 
*/auth* request),
- `timestamp` **float**, current timestamp, 
- `data` **array**, specific data needed for the request.


## Responses

Body of every response is a valid JSON string. Every response uses the format below. All listed fields will be present
even if they are empty.

```json
{
  "status": 1,
  "message": "success",
  "data": {}
}
```

- `status` **bool**, represents the overall result of the request. 1 if the request was successful, 0 if the request 
failed.
- `message` **string**, verbose representation of `status`.
- `data` **array**, all custom data for every request are stored here.


## Objects

Every object has two mandatory fields:  `id` and `permissions`.

```json
{
  "id": "abc123",
  "permissions": ["view", "edit", "delete", "assign"],
  "...": "..."
}
```

- `id` **string**, unique identifier of the object, is used when modifying or deleting the object
- `permissions` **array**, list of possible actions the used *auth_token* cen perform with the object
    + `view` always present, user can see the object
    + `edit` user can modify the object's parameters
    + `delete` user can remove the object
    + `assign` present only in some objects, user is owner of the object and they can pass the ownership to someone else
-->