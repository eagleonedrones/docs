
# Location [/location]

## Object definition

```javascript
{
    "id": "a1",
    "name": "Location #1",
    "geo_sw_lat": 50.0964759,
    "geo_sw_lng": 14.3937703,
    "geo_ne_lat": 50.1019541,
    "geo_ne_lng": 14.4088336,
    "members_count": 5,
    "devices_count": 3,
    "owner_id": "abc123"
}
```
- `name` **string**, name of the object
- `location` **array**, specifies the allowed geo boundaries for the location
    + `sw` **array**, the south-west corner
    + `ne` **array**, the north-east corner
        + `lat` **float**, latitude
        + `lng` **float**, longitude

- `members_count` **numeric**, number of members in the location
- `devices_count` **numeric**, number of devices in the location
- `owner_id` **string**, id of the user who owns the location


## Create new [POST]

+ Request (application/json)

        {
            // TODO: tohle patří do hlavičky
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            // TODO: tady timestamp nepotřebujeme (requesty platí to timeoutu)
            "timestamp": 1549292026,
            "data": {
                "id_user": "a1",
                "name": "New location name"
            }
        }

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "123",
                "name": "New group name",
                "location": {
                    "sw": {
                        "lat": "",
                        "lng": ""
                    },
                    "ne": {
                        "lat": "",
                        "lng": ""
                    }
                },
                "members_count": 1,
                "devices_count": 0,
                "id_user": "a1",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

## Get all [GET /group?load_linked_objects={load_linked_objects}]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + load_linked_objects (bool)
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "1",
                    "name": "Group #1",
                    "location": {
                        "sw": {
                            "lat": 50.0964759,
                            "lng": 14.3937703
                        },
                        "ne": {
                            "lat": 50.1019541,
                            "lng": 14.4088336
                        }
                    }, 
                    "members_count": 5,
                    "devices_count": 3,
                    "id_user": "1",
                    "user": {},
                    "permissions": ["view", "edit", "delete", "assign"]
                },
                {
                    "id": "2",
                    "name": "Group #2",
                    "location": {
                        "sw": {
                            "lat": 50.0964759,
                            "lng": 14.3937703
                        },
                        "ne": {
                            "lat": 50.1019541,
                            "lng": 14.4088336
                        }
                    }, 
                    "members_count": 1,
                    "devices_count": 0,
                    "id_user": "1",
                    "permissions": ["view", "edit", "delete", "assign"]
                }
            ]
        }

## Group - Object [/group/{id_group}]

### Get [GET]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_group (string)
        
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "12",
                "name": "Group #1",
                "location": {
                    "sw": {
                        "lat": 50.0964759,
                        "lng": 14.3937703
                    },
                    "ne": {
                        "lat": 50.1019541,
                        "lng": 14.4088336
                    }
                }, 
                "members_count": 6,
                "devices_count": 2,
                "id_user": "1",
                "permissions": ["view", "edit", "delete", "assign"]
            }
        }

### Patch [PATCH]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "id_user": "5",
                "name": "Group new name",
                "location": {
                    "sw": {
                        "lat": 50.0964759,
                        "lng": 14.3937703
                    },
                    "ne": {
                        "lat": 50.1019541,
                        "lng": 14.4088336
                    }
                }
            }
        }

+ Parameters
    + id_group (string)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": "12",
                "name": "Group new name",
                "location": {
                    "sw": {
                        "lat": 50.0964759,
                        "lng": 14.3937703
                    },
                    "ne": {
                        "lat": 50.1019541,
                        "lng": 14.4088336
                    }
                },
                "members_count": 1,
                "devices_count": 0,
                "id_user": "5",
                "permissions": ["view", "edit", "delete"]
            }
        }

### Update [PUT]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": {
                "id_user": "5",
                "name": "New name",
                "location": {
                    "sw": {
                        "lat": 50.0964759,
                        "lng": 14.3937703
                    },
                    "ne": {
                        "lat": 50.1019541,
                        "lng": 14.4088336
                    }
                },
            }
        }

+ Parameters
    + id_group (string)
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": {
                "id": 123,
                "name": "Group name",
                "location": {
                    "sw": {
                        "lat": 50.0964759,
                        "lng": 14.3937703
                    },
                    "ne": {
                        "lat": 50.1019541,
                        "lng": 14.4088336
                    }
                },
                "members_count": 1,
                "devices_count": 0,
                "id_user": "5",
                "permissions": ["view", "edit", "delete"]
            }
        }

### Delete [DELETE]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }

+ Parameters
    + id_group (string)
    
+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": []
        }
        
## Group - Objects linking [/group/{id_group}/]
Get other objects associated with the group.

### Get users [GET /group/{id_group}/user]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "email": "email@email.email",
                    "groups_count": 1,
                    "sessions_count": 0,
                    "role": "user",
                    "permissions": ["view"],
                    "group_role": "invited"
                },
                {
                    "id": "456",
                    "email": "second@email.email",
                    "groups_count": 3,
                    "sessions_count": 2,
                    "role": "user",
                    "permissions": ["view"],
                    "group_role": "member"
                },
                {
                    "id": "789",
                    "email": "third@email.email",
                    "groups_count": 5,
                    "sessions_count": 2,
                    "role": "user",
                    "permissions": ["view"],
                    "group_role": "owner"
                },
                {
                    "id": "916",
                    "email": "fourth@email.email",
                    "groups_count": 1,
                    "sessions_count": 1,
                    "role": "user",
                    "permissions": ["view"],
                    "group_role": "admin"
                }
            ]
        }

### Get devices [GET /group/{id_group}/device?load_linked_objects={load_linked_objects}]

+ Request (application/json)

        {
            "auth_token": "jF06GqT6ppA9eNvFDRzqS31S4lCpFpOo",
            "timestamp": 1549292026,
            "data": []
        }
        
+ Parameters
    + id_group (string)
    + load_linked_objects (bool)

+ Response 200 (application/json)

        {
            "status": 1,
            "message": "success",
            "data": [
                {
                    "id": "123",
                    "type": "sensor",
                    "name": "DeDrone array",
                    "files_count": 0,
                    "id_group": 5,
                    "id_user": 1,
                    "user": {
                        "id": 123,
                        "email": "email@email.email",
                        "groups_count": 0,
                        "sessions_count": 0,
                        "role": "user",
                        "permissions": ["view", "edit"],
                        "group_role": "invited"
                    }
                },
                {
                    "id": 456,
                    "type": "drone",
                    "name": "Eagle drone #1",
                    "files_count": 0,
                    "id_user": 1,
                    "id_group": 5,
                    "user": {
                        "id": 123,
                        "email": "email@email.email",
                        "groups_count": 0,
                        "sessions_count": 0,
                        "role": "user",
                        "permissions": ["view", "edit"],
                        "group_role": "invited"
                    }
                },
                {
                    "id": 789,
                    "type": "drone",
                    "name": "Eagle drone #2",
                    "files_count": 0,
                    "id_user": 1,
                    "id_group": 5,
                    "user": {
                        "id": 123,
                        "email": "email@email.email",
                        "groups_count": 0,
                        "sessions_count": 0,
                        "role": "user",
                        "permissions": ["view", "edit"],
                        "group_role": "invited"
                    }
                }
            ]
        }

