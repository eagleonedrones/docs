# Eagle HUB - stavy a přechody

*Eagle* se vždy nachází v nějakém z definovaných stavů viz níže. Některé stavy lze nastavit pomocí zprávy typu `device/setstate` a informace o aktuálním stavu chodí vždy z daného zařízení zprávou `device/info` viz [typy zpráv](./typy-zprav.md).


## Jednotlivé stavy

### `standby`

*Eagle* je v docku a je připravený k akci.

```javascript
// params
{}

// valid_states
[
	"hunter",
	"radar"
]
```


### `hunter`

*Eagle* je ve vzduchu a očekává instrukce přes `alert/mobile` viz [typy zpráv](./typy-zprav.md). Lze přepnout do módu *manuálního řízení*, do `radar` módu a nebo *misi* zrušit.

```javascript
// params
{}

// valid_states
[
	"radar",
	"manualcontrol",
	"missionfailed"
]
```


### `radar`

*Eagle* je v předepsané *výšce* a sleduje okolí v předepsaném *perimetru*. Toto nastavení je možné upravit, stejně tak je možné *misi* zrušit a nebo se přepnout do módu *manuálního řízení*, nebo do `hunter` módu pro navedení na cíl pomocí mobilní aplikace.

```javascript
// params
{
	"alt": 120,  // v metrech
	"perimeter": 50
}

// valid_states
[
	"hunter",
	"manualcontrol",
	"missionfailed"
]
```


### `manualcontrol`

*Eagle* je *manuálně* ovládán přes mobilní aplikaci a v tuto chvíli lze ovládat pouze *uživatelem*, jenž je právě *pilolem* a nelze ovládat nikým jiným.

```javascript
// params
{
	"pilot_username": "person.in.control@drone.empire"
}

// valid_states
[]
```


### `followingtarget`

*Eagle* je v *autonomním módu* a sleduje cíl. Je možné *misi* zrušit návratem do původního stavu `hunter` nebo `radar`, odkud je pak možné *misi* dále zrušit nebo jej na cíl navádět *manuálně*.

```javascript
// params
{
	"target_distance": 15.2  // v metrech
}

// valid_states - vždy bude přítomen právě jeden
[
	"hunter",
	"radar"
]
```


### `missionsucceed`

*Mise* proběhla úspěšně, dron narušitel byl chycen, nebo jinak zneškodněn. Je-li nastavená výchozí akce, *Eagle* ji automaticky provádí, případně je potřeba nějakou akci zvolit nebo dodatečně změnit nebo zvolit mód *manuálního řízení*.

```javascript
// params
{
	"current_action": null,
	"valid_actions": [
		"land_home",
		"land_startpoint",
		"land_immediately"
	]
}

// valid_states
[
	"missionsucceed",
	"manualcontrol"
]
```


### `missionfailed`

*Mise* nebyla úspěšně dokončená - cíl nebyl nalezen nebo byla zrušena *uživatelem*. Je-li nastavená výchozí akce, *Eagle* ji automaticky provádí, případně je potřeba nějakou akci zvolit nebo dodatečně změnit nebo zvolit mód *manuálního řízení*.

```javascript
// params
{
	"current_action": null,
	"valid_actions": [
		"land_home",
		"land_startpoint",
		"land_immediately"
	]
}

// valid_states
[
	"missionfailed",
	"manualcontrol"
]
```


### `actionneeded`

*Eagle* je na zemi a je potřeba součinnost *uživatele* - např. nabít dělo se sítí, vložit *Eagla* správně do docku a zkontrolovat, zda není nějakým způsobem poškozen.

Params `netgun_ready` a `battery.charging` jsou shodné s hodnotami v `device/info`. Ve chvíli kdy jsou obě ve stavu `true`, *Eagle* se sám přepne do stavu `standby`.

```javascript
// params
{
	"netgun_ready": false,
	"battery.charging": true
}

// valid_states
[]
```


#TODO: ERROR state(s)







