# Eagle HUB - typy zpráv

Jednotlivé typy zpráv, které se přes HUB posílají, jejich formát, jejich producenti a jejich konzumenti.


## Obecný tvar

### Odesílané zprávy

Je možné (volitelně) definovat `receiver_id` pro odeslání zprávy konkrétnímu / jedinému zařízení, nebo `location_id` pro odeslání zprávy všem zařízením v dané oblasti, nikoli však definovat současně.

```javascript
{
	"emmited": 12345678, // unix timestamp vzniku události (ve vteřinách)
	"type": "{typ zprávy}",
	"data": {
		// payload pro daný typ zprávy (viz níže)
	},
	"receiver_id": "{id zařízení}", // volitelně
	"location_id": "{id lokality}" // volitelně
}
```	


### Příchozí zprávy

```javascript
{
	"emitted": 12345678, // unix timestamp vzniku události (ve vteřinách)
	"stored": 12345681, // unix timestamp přijetí zprávy do HUBu (ve vteřinách)
	"sender_id": "{id zařízení}",
	"type": "{typ zprávy}",
	"data": {
		// payload pro daný typ zprávy (viz níže)
	},
}
```


## Jednotlivé typy zpráv

### `push/entity`

**Tigger:** Backoffice pošle push notifikaci prostřednictvím *HUBu* o vzniku nové, nebo změny stávající, entity s daty ve formátu shodném s [*REST API*](../backoffice/).

```javascript
{
	"entity_path": "/companies/42/locations/43/",
	"entity_data": {
		"id": 43,
		"company_id": 42,
		"name": "Císařský Ostrov",
	    "geo_sw_lat": 50.0964759,
	    "geo_sw_lng": 14.3937703,
	    "geo_ne_lat": 50.1019541,
	    "geo_ne_lng": 14.4088336,
	    "owner_id": "abc123",
	    "devices": [
	    	{
	    		"xx": xx
	    	},
	    	// ...
		]
	}
}
```


### `alert/dedrone`

**Trigger:** *DeDrone* detekuje objekt, ev. detekovaná pozice objektu se změní.

```javascript
{
	"alert_id": 12, // při detekci více objektů současně je nutné na cílovém
	"ltd": 14.156,  // zařízení tyto informace spojovat dle `alert_id`
	"lng": 29.234,
	"alt": 50
}
```


### `alert/mobile`

**Trigger:** *Mobilní telefon* v *Hunter módu* vysílá informace o jeho natočení, tj. směr kam ukazuje.

```javascript
{
	"polar": 21.42,  // úhel
	"azimuth": 124.423444, // úhel
	"distance": 0.27  // rozsah 0.00 - 1.00
}
```


### `device/info`

**Trigger:** Každý *Eagle* při jakékoli změně informací pošle update o svém stavu.

```javascript
{
	"device_id": "{device_id}",
	"settings": [
		{
			"icon": "targeting",
			"message": "You should find better target!"
		},
		{
			"icon": "fallback",
			"message": "Try to destroy drone."
		}
	],
	"battery": {
		"charging": true,
		"percentage": 25,
		"time_left": "00:23",  // HH:MM
	},
	"netgun_ready": true,
	"current_state": {
		"name": "standby",
		"params": {
			"note": "Device is ready"
		}
	},
	"valid_states": [
		"hunter",
		"radar"
	]
}
```


### `device/location`

**Trigger:** Každý *Eagle* během mise (aktivní *Hunter mód* nebo *Radar mód*) posílá tyto informace 1x / 1 s, mimo misi pak 1x / 30 s.

*Vhodné pro healthcheck.*

```javascript
{
	// ping lze vypočítat z rozdílu `emitted` a `stored`
	"device_id": "{device_id}",
	"lat": 234.234,
	"lng": 223.112,
	"alt": 15.1
}
```


### `device/setstate`

**Trigger:** Konkrétnímu zařízení (*Eaglovi*) se pošle zpráva s nastavením. Nejčastěji odesílá mobilní aplikace.

```javascript
// definovaný `receiver_id`
{
	"name": "radar",
	"params": {
		"perimeter": 130,
		"alt": 70
	}
}
```


### `device/manualcontrol`

**Trigger:** Manuální ovládání přes mobilní telefon při každé změně polohy "joysticků" vyšle jejich aktuální polohu ve všech třech osách.

Rozsah vychýlení joysticků je `0.00 - 1.00`.

```javascript
// definovaný `receiver_id`
{
	"x": -0.2, // dopředu  /  dozadu
	"y": 0,    // doprava  /  doleva
	"z": 1     // nahoru   /  dolů
}
```








